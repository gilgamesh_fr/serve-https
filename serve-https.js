/*
MIT License

Copyright (c) 2020 Open Time-Machine

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

let fs = require('fs'),
https = require('https'),
express = require('express'),
app = express();

// default values  -----------------------------------------------------------

let credential_path = "/etc/letsencrypt/live/domainname";
let https_port      = 443; // default https port value
let static_path     = "./static"
// Parameters --------------------------------------------------------------- 

function showHelp(){
    console.log( 'Command line  : node serve-https [--port=port] [--credentialpath=path] [--staticpath=path] [--help]\n');
    process.exit();
}

const  processArgs = process.argv.slice(2);

processArgs.forEach(e => {
    const p = e.split('=');
    switch (p[0]) {
        case '--port'           : https_port      = p[1] ;  break;
        case '--credentialpath' : credential_path = p[1] ;  break;
        case '--staticpath'     : static_path     = p[1] ;  break;
        case '--help'           : showHelp() ;  break;
    }
});

// Application setting --------------------------------------------------------

app.use(express.static(static_path));

// Start server  --------------------------------------------------------------

let credentials = {
	key:  fs.readFileSync(credential_path+'/privkey.pem', 'utf8'),
	cert: fs.readFileSync(credential_path+'/cert.pem',    'utf8'),
	ca:   fs.readFileSync(credential_path+'/chain.pem',   'utf8')
};


let httpsServer = https.createServer(credentials, app);
httpsServer.listen(https_port, () => { 
	console.log('HTTPS Server running on port   :'+ https_port);
	console.log('HTTPS Server credential path   :'+ credential_path);
	console.log('HTTPS Server static files path :'+ static_path);
	
});
